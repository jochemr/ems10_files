import math
def wortel_verglijking(a, b, c):
    D = math.sqrt(b**2)-(4*a*c)
    print("Discriminant is ", D)
    
    if D == 0:
        X1 = ((-b + math.sqrt(D)) //2*a)
        print("X1 = ",X1)
        X2 = ((-b - math.sqrt(D))//2*a)
        print("X2 = ",X2)
    
    
    if D < 0:
        print("Deze vierkantsvergelijking heeft geen reële wortels.")
    if D == 0:
        print("Deze vierkantsvergelijking heeft één reële wortels.")
    if D > 0:
        print("Deze vierkantsvergelijking heeft twee reële wortels.")
        
wortel_verglijking(8, 6, 3)