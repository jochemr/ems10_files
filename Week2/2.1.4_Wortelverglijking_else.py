import math
def wortel_verglijking(a, b, c):
    D = math.sqrt((b**2)-(4*a*c))
    print("Discriminant is ", D)  
    
    if D < 0:
        print("Deze vierkantsvergelijking heeft geen reële wortels.")
    elif D == 0:
        print("Deze vierkantsvergelijking heeft één reële wortels.")
    else:
        print("Deze vierkantsvergelijking heeft twee reële wortels.")
        
    if D == 0 or D > 0:
        X1 = ((-b + D)) / (2*a)
        print("X1 = ",X1)
        X2 = ((-b - D)) / (2*a)
        print("X2 = ",X2)
        
wortel_verglijking(2, 5, -7)