def line():
    print('-'*40)

def sides():
    print('|'+' '*38 +'|')
    
def box():
    line()
    sides()
    sides()
    sides()
    line()
    
box()
    