def line(width):
    print(width * '-')
    
def sides(width):
    print('|'+width*' '+'  |')
    
def box(width, height):
    line(width)
    for i in range(4):
        sides(height)
    line(width)
    
box(10, 6)