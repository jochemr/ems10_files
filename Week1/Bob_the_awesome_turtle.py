import math
import turtle
bob = turtle.Turtle()
t = bob
def polygon(t, n, length):
    angle = 360 / n
    for i in range(n):
        t.fd(length)
        t.lt(angle)

def circle(t, r):
    circumference = 2 * math.pi * r
    n = 50
    length = circumference / n
    polygon(t, n, length)

for f in range(15):
    circle(t, 10*f)
    