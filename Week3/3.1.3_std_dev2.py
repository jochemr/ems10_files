import math
import statistics
test_list = [3.2, 5.7, 8.5, 9.1]

def add_all(t):
    total = 0
    for x in t:
        total += x
    return total

def avg(t):
    average = add_all(t)/len(test_list)
    return average

def std_dev(t):
    u = 0
    n = len(test_list)
    for i in range(n):
        u = u +((1/n)*test_list[i])
    mu = 0
    for i in range(n):
        mu = mu +((1/n)*((test_list[i]-u)**2))    
    x = math.sqrt(mu)
    return x
    
print(std_dev(test_list))
print(statistics.mean(test_list))
if statistics.mean(test_list)==std_dev(test_list):
    print('Gelijk')
else:
    print('Niet gelijk')

